use std::borrow::ToOwned;
use std::collections::HashSet;

use anyhow::Result;
use itertools::Itertools;
use pdbtbx::{Atom, Residue, PDB};
use rayon::iter::FromParallelIterator;
use rayon::prelude::ParallelIterator;

type AtomList = Vec<usize>;
type ResidueList = Vec<isize>;

/// Takes a comma-separated list (usually from command line input) as string and parses it into
/// a vector of Atom IDs. The Input may be atom IDs or Atom Names
pub fn parse_atomic_list(input: &str, pdb: &PDB) -> Result<AtomList, anyhow::Error> {
    let input_vec: Vec<&str> = input.split(',').collect();
    let input_set: HashSet<String> = input_vec.iter().map(|s| s.to_lowercase()).collect();

    let pdb_set: HashSet<String> =
        HashSet::from_par_iter(pdb.par_atoms().map(|a| a.name().to_lowercase()));
    let mut missing_atoms = input_set.difference(&pdb_set).peekable();

    ensure!(
        missing_atoms.peek().is_none(),
        "No atom(s) found with identifier(s): {}",
        missing_atoms.format(",")
    );

    let output_vec = pdb
        .par_atoms()
        .filter(|x| {
            input_vec
                .iter()
                .any(|y| x.name().to_lowercase() == y.to_lowercase())
        })
        .map(Atom::serial_number)
        .collect();

    Ok(output_vec)
}

/// Parses a string (usually taken from command line) and returns a list of residues given by a tuple
/// of serial numbers and insertion codes. The input can be either a comma-separated list of serial numbers
/// and insertion codes or residues names.
pub fn parse_residue_list(input: &str, pdb: &PDB) -> Result<ResidueList, anyhow::Error> {
    let residue_set: HashSet<String> = input.split(',').map(ToOwned::to_owned).collect();

    let pdb_set: HashSet<String> =
        HashSet::from_par_iter(pdb.par_residues().map(|a| a.name().unwrap().to_lowercase()));
    let mut missing_residues = residue_set.difference(&pdb_set).peekable();

    ensure!(
        missing_residues.peek().is_none(),
        "No residue(s) found with identifier(s): {}",
        missing_residues.format(",")
    );

    Ok(pdb
        .par_residues()
        .filter(|r| residue_set.contains(&r.name().unwrap_or("").to_lowercase()))
        .map(Residue::serial_number)
        .collect())
}

#[cfg(test)]
mod tests {
    use super::*;
    use pdbtbx::StrictnessLevel;

    fn test_pdb(path: &str) -> PDB {
        let (pdb, _) = pdbtbx::open_pdb(path, StrictnessLevel::Strict).unwrap();
        pdb
    }

    #[test]
    fn parse_atomic_list_test() {
        // let num_list = "1,2:5,7,9-11";
        let str_list = "OH,HH";
        let pdb = test_pdb("tests/test_blank.pdb");

        // assert_eq!(
        //     parse_atomic_list(num_list, &pdb).unwrap(),
        //     vec!(1, 2, 3, 4, 5, 7, 9, 10, 11)
        // );
        assert_eq!(parse_atomic_list(str_list, &pdb).unwrap(), vec![39, 40]);
    }

    #[test]
    fn parse_residue_list_test() {
        // let num_list = "1,2:5,6-7";
        let str_list = "gly,wat";
        let pdb = test_pdb("tests/test_blank.pdb");

        // assert_eq!(
        //     parse_atomic_list(num_list, &pdb).unwrap(),
        //     vec!(1, 2, 3, 4, 5, 6, 7)
        // );
        assert_eq!(parse_residue_list(str_list, &pdb).unwrap(), vec![2, 6, 7])
    }
}
